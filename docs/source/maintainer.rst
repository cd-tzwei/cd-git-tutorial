Maintainer
==========

This section is for Maintainer operations

GitLab
------

Review & Merging the Merge Requests
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

#. In Merge Requests page as default showing list of all requests made by developers page

    .. image:: images/mr-list.png
        :align: center

#. Click on the merge request title will go to the detail page

    .. image:: images/mr-details.png
        :align: center

#. Click ``Merge`` If no conflict. ``Delete source branch`` is option and recommend to select.

    .. hint:: If having conflict will showing:

        .. image:: images/mr-conflict.png
            :align: center

        Maintainer can choose:
            - ``Resolve conflicts`` from here solve in web
            - ``Merge locally`` will show command to merge in local and solve in local and push again
            - Comment this merge request and ask Developer to solve the conflict and push again to that branch


Filter List
^^^^^^^^^^^

In merge request list Maintainer can filter the list via the searh field

.. image:: images/mr-filter-1.png
        :align: center

.. image:: images/mr-filter-2.png
        :align: center
