﻿Workflow
=========

Workflow for project

.. image:: images/workflow.png
    :align: center

Git Flow
========

Git flow for project

.. figure:: images/gitflow.png
   :align: center
