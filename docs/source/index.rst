.. git-tutorial documentation master file, created by
   sphinx-quickstart on Fri Oct  2 15:34:36 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to git-tutorial's documentation!
========================================

This tutorial is showing git operations using sourcetree

Contents
^^^^^^^^

.. toctree::
   :maxdepth: 2

   workflow
   developer
   maintainer

Sourcetree UI
=============

This UI based on Sourcetree 3.3.9 Windows

.. figure:: images/ui.png
   :align: center

Example of a good practice of Git branching
===========================================

.. figure:: images/git-branching-model.png
   :align: center
