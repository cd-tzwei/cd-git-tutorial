project = 'cd-git-tutorial'
copyright = '2020, Tzwei'
author = 'Tzwei'
master_doc = 'index'

extensions = [
    'sphinx_rtd_theme'
]

html_theme = 'sphinx_rtd_theme'