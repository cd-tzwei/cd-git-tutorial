Developer
=========

This section is for Developer operations

Sourcetree
----------

Sourcetree - New branch (start a new feature)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When starting a new feature or start a new bug fixing, **MUST** branching out from ``pre_live`` branch:

#. Right click on ``REMOTES > origin > pre_live``, click on ``checkout origin/pre_live``
#. At ``New local branch name`` write ``feature/feature-name`` (please remember to start with ``feature/``) then click ``OK``

.. image:: images/new-branch.png
    :align: center

Sourcetree - Commit Changes
^^^^^^^^^^^^^^^^^^^^^^^^^^^

At the **FIRST** time when commit the changes **REMEMBER** to tick ``Push changes immediately to -`` to let the branches pushed to origin

After the **FIRST** time can choose to not tick the ``Push changes immediately to -``

.. image:: images/commit.png
    :align: center

Sourcetree - Push branch
^^^^^^^^^^^^^^^^^^^^^^^^

After code is done, and committed in local, need to push code to remote.

#. Right click on the ``REMOTES > feature > feature-1``
#. Choose ``Push to > origin``

.. image:: images/push-to-remote.png
    :align: center

.. tip::
    If miss out tick the checkbox in commit stage and the branch is no showing in push window, like

    .. image:: images/sample-cant-push.png
        :align: center
        :scale: 20%

    can choose to (pick only one method below):

        A. Execute following command in terminal::

            git push --set-upstream origin feature/feature-1

        B. Amend last commit message and tick the checkbox agn

        .. image:: images/amend.png
            :align: center

    .. attention:: If a commit is already on remote, please dont simply amend commit message, unless you know what you are doing!!!

Sourcetree - Delete branch
^^^^^^^^^^^^^^^^^^^^^^^^^^

After branch is deleted in remote or when want to remove branch

#. Be sure **NOT** in the branch which need to delete
#. Right click on the branch name, choose ``Delete branch/name`` and tick ``Force delete`` for prevent it from error (When got changes stuck in the branch is cannot normal delete branch)

.. image:: images/delete.png
    :align: center

Sourcetree - Conflict when merging Feature branch to Staging branch
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Sometimes **staging** has many different commit ahead compared to **feature(prelive)** branch will happen conflict. Since we do not want to make changes on others' changes, so we need to "discard" those file changes that not changes in current **feature** branch.

#. Ensuring those conflict files are not the files you changed

    .. image:: images/conflict.png
        :align: center

#. Right click on the **unstaged** conflicted file then click ``Resolve Conflicts > Resolve using 'Mine'``, repeat on those conflicted files

    .. image:: images/resolve-conflict.png
        :align: center

#. Commit and Push. **DONE**

Git Command
-----------

Git Command - New branch (start a new feature)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    git checkout -b feature/feature-1 --track origin/master

Git Command - Commit Changes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    # stage all file
    git add .
    # commit with message
    git commit -m "Done feature"

Git Command - Push branch
^^^^^^^^^^^^^^^^^^^^^^^^^

::

    git push --set-upstream origin feature/feature-1

Git Command - Delete branch
^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

    # change branch to other branch
    git checkout master
    # force delete local branch
    git branch -D feature/feature-1

GitLab
------

After branch was pushed to ``REMOTES``, developer need to create ``PUSH REQUEST`` in GitLab

Git Command - Create Merge Request
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Inside the GitLab repository project page, click the following buttons

.. image:: images/merge-request-1.png
    :align: center

After clicked ``New merge request``, then
    #. At ``Source branch`` panel, choose feature branch
    #. At ``Target branch`` panel, choose ``staging``/``pre_live``
    #. Click ``Compare branches and continue``

.. image:: images/merge-request-2.png
    :align: center

After clicked ``Compare branches and continue``
    #. Fill in the fields
    #. When merging to
        - ``STAGING``, **untick** the ``Delete source branch when merge request is accepted.``
        - ``PRE_LIVE``, **tick** the ``Delete source branch when merge request is accepted.``
    #. Click ``Submit merge request``

.. image:: images/merge-request-3.png
    :align: center

.. tip::
    After the merge request submitted still pushing a new commit, the same request will be follow the latest branch status automatically
